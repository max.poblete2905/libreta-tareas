requirer('colors');

const showMenu = () => {

    return new Promise(resolver => {
        console.clear()
        console.log('================================='.green)
        console.log('          Select Opcions      '.green)
        console.log('=================================\n'.green)

        console.log(`${'1.'.green} create task`)
        console.log(`${'2.'.green} list tash`)
        console.log(`${'3.'.green} list completed tash`)
        console.log(`${'4.'.green} list task`)
        console.log(`${'5.'.green} complete task`)
        console.log(`${'6.'.green} delete tash`)
        console.log(`${'0.'.green} exit\n`)

        const readline = require('readline').createInterface({
            input: process.stdin,
            output: process.stdout,
        });

        readline.question('Select Options : ', (opt) => {

            readline.close();
            resolver(opt)
        })
    })
}

const stop = () => {

    return new Promise(resolver => {
        const readline = require('readline').createInterface({
            input: process.stdin,
            output: process.stdout,
        });

        readline.question(`\nPress ${'ENTER'.green}  to continue\n`, (opt) => {
            readline.close();
            resolver()
        })
    })
}

module.exports = {
    showMenu,
    stop,
}